import 'package:cook_app/src/service/global.dart';
import 'package:cook_app/src/service/providers.dart';
import 'package:cook_app/src/service/routes.dart';
import 'package:cook_app/src/service/theme.dart';
import 'package:cook_app/src/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:renovation_core/core.dart';

main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Global.init();
  final renovationInstance = Renovation();
  var appDataPath = await Global().getApplicationDataPath();
  await renovationInstance.init(apiUrl, cookieDir: '$appDataPath/cookies');

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: appName,
        theme: themData,
        initialRoute: '/',
        routes: routes,
      ),
    );
  }
}