import 'package:cook_app/src/service/size_config.dart';
import 'package:cook_app/src/view/common_widgets.dart';
import 'package:cook_app/src/view_model/auth_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatelessWidget {
  static String routeName = "/profile";

  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        appBar: buildAppBar(context),
        body: FutureBuilder(
            future: getUserDetails(context),
            builder: (BuildContext ctx,AsyncSnapshot snapshot) {
              if(snapshot.connectionState == ConnectionState.done)
              {
                if(snapshot.hasData ){
                  _nameController.text = snapshot.data['user_name'] ?? '';
                  _phoneController.text = snapshot.data['email'] ?? '';
                  return Column(
                    children: [
                      Padding(
                        padding:  EdgeInsets.all(SizeConfig.height*5),
                        child: Center(
                          child: SizedBox(
                            height: SizeConfig.safeHeight*30,
                            width: SizeConfig.safeWidth*60,
                            child: CircleAvatar(
                              backgroundImage: AssetImage('assets/images/profile.jpeg'),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding:  EdgeInsets.symmetric(horizontal: 20),
                        child: buildTextFormField(controller: _nameController,label: 'Name'),
                      ),
                      Padding(
                        padding:  EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                        child: buildTextFormField(controller: _phoneController,label: 'Phone number'),
                      ),
                    ],
                  );
                }
                else
                  return Center(child: CircularProgressIndicator());
              }
              else
                return Center(child: CircularProgressIndicator());
            }
        ));
  }

  TextFormField buildTextFormField({@required TextEditingController controller,@required String label}) {
    return TextFormField(
      // validator: isEmptyValidator,
      controller: controller,
      readOnly: true,
      // enabled: (provider.user.editedIndex ?? 0) >= 2 ? false : true,
      decoration: InputDecoration(
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: Color(0xff263238))),
          //labelStyle: styling.mulishTextStyle(FontWeight.w800, null, null),
          labelText: label),
    );
  }

  Future<Map<String,String>> getUserDetails(BuildContext context) async{
    return  {
      "user_name": "Ilyas Arafath",
      "email": " abc@mail.com"
    };
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    title: Text('Profile'),
    actions: [
      IconButton(onPressed: ()async{
        bool _confirmed = await showConfirmDialog(context:context,
            title: 'Logout!',message: 'Are you sure want to Logout?');
        if(_confirmed != null && _confirmed){
          context.read<AuthProvider>().signOut(context);
        }

      },
          icon:Icon(Icons.exit_to_app) )
    ],
  );
}