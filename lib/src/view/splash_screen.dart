

import 'dart:async';

import 'package:cook_app/src/service/global.dart';
import 'package:cook_app/src/utilities/constants.dart';
import 'package:cook_app/src/view/common_widgets.dart';
import 'package:cook_app/src/view/main_screen.dart';
import 'package:cook_app/src/view/welcome_page/welcome_page.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  static String routeName = "/";

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}


class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    Timer(Duration(seconds: 1), (){
      if(Global.localStorage.getBool(LOGIN_SUCCESS) == null ||
          Global.localStorage.getBool(LOGIN_SUCCESS) == false){
        //login screen
        Navigator.pushReplacementNamed(context, WelcomePage.routeName);
      }
      else {
        Navigator.pushReplacementNamed(context, MainScreen.routeName);
      }
    }
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      height: double.infinity,
      child: loader(),
    );
  }
}