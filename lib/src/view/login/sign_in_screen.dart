
import 'package:cook_app/src/service/size_config.dart';
import 'package:cook_app/src/utilities/colors.dart';
import 'package:cook_app/src/utilities/strings.dart';
import 'package:cook_app/src/view/login/widgets/sign_form.dart';
import 'package:cook_app/src/view/signup/signup_screen.dart';
import 'package:flutter/material.dart';

class SignInScreen extends StatelessWidget {
  static String routeName = "/sign_in";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign In"),
      ),
      body: Body(),
    );
  }
}

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
          EdgeInsets.symmetric(horizontal: SizeConfig.width*7),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.height*8),
                Text(
                  "Welcome Back",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: SizeConfig.height*2),
                Text(signWithEmail,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height:SizeConfig.height*5),
                SignForm(),
                SizedBox( height:SizeConfig.height*5),

                SizedBox(height:SizeConfig.height*0.1),
                NoAccountText(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class NoAccountText extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Don’t have an account? ",
          style: TextStyle(fontSize: 16),
        ),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, SignUpScreen.routeName),
          child: Text(
            "Sign Up",
            style: TextStyle(
                fontSize: 16,
                color: darkRed),
          ),
        ),
      ],
    );
  }
}
