import 'package:cook_app/src/service/size_config.dart';
import 'package:cook_app/src/utilities/colors.dart';
import 'package:cook_app/src/utilities/constants.dart';
import 'package:flutter/material.dart';

class WelcomeSwipe extends StatelessWidget {
  const WelcomeSwipe({
    Key key,
    this.text,
    this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        Text(
          appName,
          style: TextStyle(
            fontSize: 30,
            color: colorPrimary,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18),
        ),
        Spacer(flex: 2),
        Image.asset(
          image,
          height: SizeConfig.width*70,
          width: SizeConfig.width*80,
        ),
      ],
    );
  }
}
