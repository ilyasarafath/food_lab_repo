import 'package:cook_app/src/service/global.dart';
import 'package:cook_app/src/service/size_config.dart';
import 'package:cook_app/src/utilities/constants.dart';
import 'package:cook_app/src/view/common_widgets.dart';
import 'package:cook_app/src/view/login/sign_in_screen.dart';
import 'package:cook_app/src/view_model/auth_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  String _fName;
  String _lName;
  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Form(
      key: _formKey,
      child: Column(
        children: [

          buildFirstName(),
          SizedBox(height:SizeConfig.height*5),
          buildLastName(),
          SizedBox(height:SizeConfig.height*5),
          buildEmailFormField(),
          SizedBox(height:SizeConfig.height*5),
          buildPasswordFormField(),
          SizedBox(height:SizeConfig.height*5),
          // FormError(errors: errors),
          SizedBox(height:SizeConfig.height*5),
          DefaultButton(
            text: "SignUp",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                Global.hideKeyboard(context);
                context.read<AuthProvider>().registerUser(context, fName: _fName,
                    lName: _lName, email: _email,
                    password: _password);
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => _password = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return "Please enter password";
        } else if (value.length < 5) {
          return "Password should be more than 5 character needed";
        }
        return null;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        labelText: "Password",
        hintText: "Enter your password",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.lock),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => _email = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return "Please enter email";
        } else if (!value.contains('@')) {
          return "Email format is incorrect";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Email",
        hintText: "Enter your email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.mail_outline),
      ),
    );
  }

  TextFormField buildFirstName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      onSaved: (newValue) => _fName = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return "Please enter first name";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "First Name",
        hintText: "Enter your first name",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.account_box),
      ),
    );
  }

  TextFormField buildLastName() {
    return TextFormField(
      keyboardType: TextInputType.name,
      onSaved: (newValue) => _lName = newValue,
      validator: (value) {
        if (value.isEmpty) {
          return "Please enter last name";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Last Name",
        hintText: "Enter your last name",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.account_box),
      ),
    );
  }
}
