

import 'package:cook_app/src/view/favourite_screen.dart';
import 'package:cook_app/src/view/home/home_screen.dart';
import 'package:cook_app/src/view/profile_screen.dart';
import 'package:cook_app/src/view/search_screen.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  int screenIndex;
  MainScreen({Key key,this.screenIndex});
  static String routeName = "/main";

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  int _index;

  @override
  void initState() {
    _index = widget.screenIndex ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget child;
    switch (_index) {
      case 0:
        child = HomeScreen();
        break;
      case 1:
        child = SearchScreen();
        break;
      case 2:
        child = FavouriteScreen();
        break;
      case 3:
        child = ProfileScreen();
        break;
    }

    return Scaffold(
      body: SizedBox.expand(child: child),
      bottomNavigationBar: bottomNavigation()
    );
  }

  Widget bottomNavigation(){
    return  CurvedNavigationBar(
      color: Colors.white,
      backgroundColor: Colors.transparent,
      buttonBackgroundColor: Colors.redAccent,
      height: 50,
      index: _index,
      onTap: (newIndex) {
        setState(() {
          _index = newIndex;
        });
      },
      items: <Widget>[
        Icon(
          Icons.home,
          size: 26,
          color: Colors.black,
        ),
        Icon(
          Icons.search,
          size: 26,
          color: Colors.black,
        ),
       SizedBox(
         height: 22,width: 22,
           child: Image.asset('assets/images/save.png')),
        Icon(
          Icons.account_circle,
          size: 26,
          color: Colors.black,
        ),
      ],
    );
  }
}
