import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FavouriteScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Favorite Recipes'),),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: SvgPicture.asset(
                "assets/images/coming_soon.svg",
                height: 100,
                width: 100,
              ),
            ),
            SizedBox(height: 20,),
            Text('Coming soon',style: TextStyle(
                fontFamily: 'Mulish',
                fontSize: 30
            ),)
          ],
        ));
  }
}
