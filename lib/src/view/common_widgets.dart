import 'package:cook_app/src/service/size_config.dart';
import 'package:cook_app/src/utilities/colors.dart';
import 'package:cook_app/src/utilities/styles.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

showSnackBar(BuildContext context,{@required String msg}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content:   Text(msg != null ? msg : "Something Wrong"),
    duration: const Duration(seconds: 1),
  ));
}

messageDialogue(BuildContext context,{@required String content,String title=''}) {
  AlertDialog alert = AlertDialog(
    title: Text(title),
    content: Text(content),
    actions: [
      TextButton(onPressed:() => Navigator.pop(context), child: Text('Ok',style: TextStyle(
          color: Colors.lightBlue
      ),))
    ],
  );
  showDialog(
    context:context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Future<bool> showConfirmDialog({@required BuildContext context,String title,
  @required String message,String okBtnTxt,String cancelBtnTxt}) async {
  bool confirmed = await showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text(title ?? '',
            style: mulishTextStyle(color:Colors.black,fontSize: 18,fontWeight: FontWeight.w600)),
        content: Text(message,
            style: mulishTextStyle(color:Colors.black,fontSize: 16,fontWeight: FontWeight.w400)),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.pop(context, true);
              },
              child: Text(okBtnTxt ?? 'Yes',
                  style: mulishTextStyle(color:
                  Color.fromRGBO(5, 0, 246, 1),fontSize: 16,fontWeight: FontWeight.w400))),
          TextButton(
              onPressed: () {
                Navigator.pop(context, false);
              },
              child: Text(cancelBtnTxt ?? 'Not now',
                  style: mulishTextStyle(color:
                  Color.fromRGBO(255, 0, 0, 1),fontSize: 16,fontWeight: FontWeight.w400)))
        ],
      ));
  return confirmed;
}

loader() {
  return Lottie.asset('assets/json/loader_lottie.json');
}


class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key key,
    this.text,
    this.press,
  }) : super(key: key);
  final String text;
  final Function press;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      width: double.infinity,
      height: SizeConfig.height*6.5,
      child: TextButton(
        style: TextButton.styleFrom(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          primary: Colors.white,
          backgroundColor: colorPrimary,
        ),
        onPressed: press as void Function(),
        child: Text(
          text,
          style: TextStyle(
            fontSize:18,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

///this loader we are using like alert dialogue for removing call Navigator.pop(context);
loaderDialogue(BuildContext context) {
  AlertDialog alert = AlertDialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    content: Container(child: Center(child: loader())),
  );
  showDialog(
    //prevent outside touch
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      //prevent Back button press
      return WillPopScope(onWillPop: () {}, child: alert);
    },
  );
}