import 'package:cook_app/src/utilities/colors.dart';
import 'package:flutter/material.dart';

class SearchBox extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return TextField(
      readOnly: true,
      textInputAction: TextInputAction.search,
      decoration: InputDecoration(
        fillColor: Colors.grey[100],
        filled: true,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20)),
        prefixIcon: Icon(Icons.search,color: redPrimary,),
        isDense: true,
        hintText: "Recipe",
        hintStyle: TextStyle(color: Colors.grey),// Added this
        contentPadding: EdgeInsets.all(8),
      ),
      onTap: (){
      },
    );
  }
}