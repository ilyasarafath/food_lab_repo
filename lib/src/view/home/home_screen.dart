import 'dart:ui';

import 'package:cook_app/src/service/size_config.dart';
import 'package:cook_app/src/utilities/enums.dart';
import 'package:cook_app/src/utilities/strings.dart';
import 'package:cook_app/src/view/common_widgets.dart';
import 'package:cook_app/src/view/home/widgets/search_box.dart';
import 'package:cook_app/src/view_model/home_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/src/provider.dart';

class HomeScreen extends StatefulWidget {


  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {


  @override
  void initState() {
   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
     context.read<HomeScreenProvider>().getRecipeList(context);
   });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Consumer<HomeScreenProvider>(
          builder: (context, provider,child) {
            if(provider.recipeFetchStatus == ProviderStatus.LOADED){
              if(provider.recipeList.isNotEmpty){
                return ListView(
                  children: [
                    Padding(
                      padding:  EdgeInsets.only(left: 15,top: 8),
                      child: Text("What do you want \nto cook today",style: TextStyle(
                          fontSize: 25,
                          fontFamily: 'Mulish',
                          fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: 18,right: 18,top: 20),
                      child: SearchBox(),
                    ),
                    Padding(
                      padding:  EdgeInsets.only(left: 18,right: 18,top: 18),
                      child: Row(
                        children: [
                          Text('Popular lunch recipes',style: TextStyle(
                              fontSize: 15,fontWeight: FontWeight.w700
                          ),),
                          Spacer(),
                          Text('View all',style: TextStyle(color: Colors.grey),)
                        ],
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.safeHeight*60,
                      child: PageView.builder(
                          controller: PageController(
                            viewportFraction: 0.60,
                            initialPage: 0,
                          ),
                          itemCount: provider.recipeList.length,
                          itemBuilder: (BuildContext context,int index){
                            return recipeItem(provider, index);
                          }),
                    )
                  ],
                );
              }else{
                return Center(child: Text(NOTHING_TO_DISPLAY),);
              }
            }else if(provider.recipeFetchStatus == ProviderStatus.ERROR){
                  return Center(child: Text(ISSUE_MESSAGE),);
            }else{
              return loader();
            }
          }
        ),

      ),
    );
  }

  Padding recipeItem(HomeScreenProvider provider, int index) {
    return Padding(
                            padding:  EdgeInsets.only(top:18),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: SizeConfig.width*50,
                                  height: SizeConfig.height*50,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                    image: DecorationImage(
                                      image: NetworkImage(provider.recipeList[index].imageUrl),
                                      fit: BoxFit.fill
                                    )
                                  ),
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Padding(
                                      padding:  EdgeInsets.all(8.0),
                                      child: Wrap(
                                        direction: Axis.vertical,
                                        children: [
                                          Text(provider.recipeList[index].name,style: TextStyle(
                                            color: Colors.white,fontSize: 15,fontWeight: FontWeight.w500
                                          ),),
                                          Text(provider.recipeList[index].totalTime,style: TextStyle(
                                              color: Colors.white,fontSize: 15,fontWeight: FontWeight.w500
                                          ),)
                                        ],
                                      ),
                                    ),
                                  ),
                                ),

                              ],
                            ),
                          );
  }
}
