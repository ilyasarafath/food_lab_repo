

import 'package:cook_app/src/view_model/auth_provider.dart';
import 'package:cook_app/src/view_model/home_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

final List<SingleChildWidget> providers = [
  ChangeNotifierProvider(create: (_) => HomeScreenProvider()),
  ChangeNotifierProvider(create: (_) => AuthProvider()),

];