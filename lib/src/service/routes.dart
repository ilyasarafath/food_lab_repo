import 'package:cook_app/src/view/login/sign_in_screen.dart';
import 'package:cook_app/src/view/main_screen.dart';
// import 'package:cook_app/src/view/sample.dart';
import 'package:cook_app/src/view/signup/signup_screen.dart';
import 'package:cook_app/src/view/splash_screen.dart';
import 'package:cook_app/src/view/welcome_page/welcome_page.dart';
import 'package:flutter/material.dart';

final Map<String, WidgetBuilder> routes = {

  '/': (_) => SplashScreen(),
  SignInScreen.routeName: (_) => SignInScreen(),
  MainScreen.routeName: (_) => MainScreen(),
  WelcomePage.routeName: (_) => WelcomePage(),
  SignUpScreen.routeName: (_) => SignUpScreen()

};