import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Global {

  static  SharedPreferences localStorage;
  static Future init() async {
    localStorage = await SharedPreferences.getInstance();
  }

  static void hideKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  Future<String> getApplicationDataPath() async {
    if (Platform.isIOS) {
      return (await getApplicationSupportDirectory()).path;
    } else {
      return (await getApplicationSupportDirectory()).path;
    }
  }

}
