
import 'package:cook_app/src/view/common_widgets.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class APIService {
  final BuildContext context;

  APIService(this.context);

  static BaseOptions opts = BaseOptions(
      connectTimeout: 20000,
      receiveTimeout: 20000
  );

  static Dio createDio() {
    return Dio(opts);
  }

  static final dio = createDio();
  static final baseAPI = dio;

  //GET METHOD
  Future<Response> getRequest( String url) async {
    try {
      Response response = await baseAPI.get(url);
      return response;
    } on DioError catch(e) {
      // Handle error
      if (DioErrorType.RECEIVE_TIMEOUT == e.type) {
        if (e.message.contains('SocketException')) {
          showSnackBar(context,msg: "Network not Available");
          return null;
        }
      }
      else if (DioErrorType.CONNECT_TIMEOUT == e.type){
        print(e.response?.statusCode);
        showSnackBar(context,msg: "No Reliable Connection");
        return null;
      }
      else
        return e.response;
    }
  }

  //POST METHOD
  Future<Response> postRequest(String url, dynamic data) async {
    try {
      Response response = await baseAPI.post(url, data: data);
      return response;
    } on DioError catch(e) {
      // Handle error
      if (DioErrorType.RECEIVE_TIMEOUT == e.type) {
        if (e.message.contains('SocketException')) {
          showSnackBar(context,msg: "Network not Available");
          return null;
        }
      }
      else if (DioErrorType.CONNECT_TIMEOUT == e.type){
        showSnackBar(context,msg: "No Reliable Connection");
        return null;
      }
      else
        return e.response;
    }
  }

  //PUT METHOD
  Future<Response> putRequest(String url, dynamic data) async {
    try {
      Response response = await baseAPI.put(url, data: data);
      return response;
    } on DioError catch(e) {
      // Handle error
      if (DioErrorType.RECEIVE_TIMEOUT == e.type) {
        if (e.message.contains('SocketException')) {
          showSnackBar(context,msg: "Network not Available");
          return null;
        }
      }
      else if (DioErrorType.CONNECT_TIMEOUT == e.type){
        showSnackBar(context,msg: "No Reliable Connection");
        return null;
      }
      else
        return e.response;
    }
  }

  //DELETE METHOD
  Future<Response> deleteRequest(String url,dynamic data) async {
    try {
      Response response = await baseAPI.delete(url,data: data);
      return response;
    } on DioError catch(e) {
      // Handle error
      if (DioErrorType.RECEIVE_TIMEOUT == e.type) {
        if (e.message.contains('SocketException')) {
          showSnackBar(context,msg: "Network not Available");
          return null;
        }
      }
      else if (DioErrorType.CONNECT_TIMEOUT == e.type){
        showSnackBar(context,msg: "No Reliable Connection");
        return null;
      }
      else
        return e.response;
    }
  }



}