
import 'package:cook_app/src/utilities/colors.dart';
import 'package:flutter/material.dart';

final themData =  ThemeData(
        primarySwatch: colorPrimaryMaterial,
        textSelectionTheme: TextSelectionThemeData(cursorColor: Colors.black),

      );