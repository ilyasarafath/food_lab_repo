// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Recipe _$RecipeFromJson(Map<String, dynamic> json) {
  return Recipe(
    id: json['id'] as int,
    name: json['name'] as String,
    totalTime: json['totalTime'] as String,
  )..imageUrl = json['imageUrl'] as String;
}

Map<String, dynamic> _$RecipeToJson(Recipe instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'totalTime': instance.totalTime,
      'imageUrl': instance.imageUrl,
    };
