import 'package:json_annotation/json_annotation.dart';
part 'recipe.g.dart';

@JsonSerializable()
class Recipe{
  int id;
  String name;
  String totalTime;
  String imageUrl;
  Recipe({this.id,this.name,this.totalTime});


    ///fromJson
  factory Recipe.fromJson(Map<String, dynamic> json) => _$RecipeFromJson(json);

  /// toJson
  Map<String, dynamic> toJson() => _$RecipeToJson(this);
}