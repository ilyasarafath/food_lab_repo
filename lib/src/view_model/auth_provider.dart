
import 'dart:collection';

import 'package:cook_app/src/service/api_service.dart';
import 'package:cook_app/src/service/global.dart';
import 'package:cook_app/src/utilities/constants.dart';
import 'package:cook_app/src/view/common_widgets.dart';
import 'package:cook_app/src/view/login/sign_in_screen.dart';
import 'package:cook_app/src/view/main_screen.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:renovation_core/core.dart';
import 'package:renovation_core/auth.dart';

class AuthProvider extends ChangeNotifier{

  login(BuildContext context,{@required String email,@required String password}) async{
    try{
      loaderDialogue(context);

      final loginResponse = await getFrappeAuthController().login(email, password);
      if (loginResponse.isSuccess) {
        Navigator.pop(context);
        Global.localStorage.setBool(LOGIN_SUCCESS, true);
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            MainScreen()), (Route<dynamic> route) => false);
      }else{
        Navigator.pop(context);
        showSnackBar(context, msg: 'Login failed');
      }
    }catch(e){
      Navigator.pop(context);
      showSnackBar(context, msg: "Login failed");
    }
  }

  signOut(BuildContext context){
    Global.localStorage.clear();
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
        SignInScreen()), (Route<dynamic> route) => false);
  }


  registerUser(BuildContext context,{@required String fName,@required String lName,
    @required String email,@required String password})async{
    try{
      loaderDialogue(context);
      APIService api = APIService(context);
      var data =
      {
        "cmd": "cookbook_backend.api.signup",
        "args": {
          "first_name": fName,
          "last_name": lName,
          "email": email,
          "password": password
        }
      };
      
      Response response = await api.postRequest(apiUrl,data);
      print(response.data);
      if(response.statusCode == SUCCESS_STATUS_CODE){
        Navigator.pop(context);
        showSnackBar(context, msg: "successfully registered");
        Navigator.pushReplacementNamed(context, SignInScreen.routeName);
      }
      //Error response
      else if(response.statusCode == FAILURE_CODE){
        Navigator.pop(context);
        showSnackBar(context, msg: "Password format is not correct");
      } else{
        Navigator.pop(context);
        showSnackBar(context, msg: response.data['message']);
      }
    }catch(e){
      showSnackBar(context, msg: "SignUp failed");
    }
  }


}