import 'package:cook_app/src/models/recipe/recipe.dart';
import 'package:cook_app/src/service/api_service.dart';
import 'package:cook_app/src/utilities/constants.dart';
import 'package:cook_app/src/utilities/enums.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';


class HomeScreenProvider extends ChangeNotifier{

  List<Recipe> recipeList = [];
  ProviderStatus recipeFetchStatus = ProviderStatus.IDLE;

  getRecipeList(BuildContext context) async{
    try{
      recipeFetchStatus = ProviderStatus.LOADING;
      notifyListeners();

      String apiUrl = 'https://run.mocky.io/v3/b91ac006-e689-48b4-891c-74bdd2f44a72';
      APIService api = APIService(context);
      Response response = await api.getRequest(apiUrl);
      // print(response);
      if(response.statusCode == API_SUCCESS_STATUS_CODE){
        // print("success");
        // print(response.data);
        response.data['data'].forEach((element){
          recipeList.add(Recipe.fromJson(element));
        });
        recipeFetchStatus = ProviderStatus.LOADED;
        notifyListeners();
      }else{
        recipeFetchStatus = ProviderStatus.ERROR;
        notifyListeners();
      }
      print(recipeList[0].imageUrl);
    }catch(e){
      recipeFetchStatus = ProviderStatus.ERROR;
      notifyListeners();
      print(e);
    }
  }

}