import 'package:flutter/material.dart';

TextStyle mulishTextStyle({FontWeight fontWeight, double fontSize, Color color}) =>
    TextStyle(
        color: color ?? Colors.black,
        fontFamily: 'Mulish',
        fontSize: fontSize,
        fontWeight: fontWeight);


final headingStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);