
import 'package:flutter/material.dart';

const Color colorPrimary = Color(0XFFfd7463);

const redPrimary = Color(0XFFff7464);
const darkRed = Color(0XFF9f1712);

Map<int, Color> colorCombination =
{
 50:Color.fromRGBO(253,116,99, .1),
 200:Color.fromRGBO(253,116,99,  .3),
 100:Color.fromRGBO(253,116,99,  .2),
 300:Color.fromRGBO(253,116,99,  .4),
 400:Color.fromRGBO(253,116,99,  .5),
 500:Color.fromRGBO(253,116,99,  .6),
 600:Color.fromRGBO(253,116,99, .7),
 700:Color.fromRGBO(253,116,99,  .8),
 800:Color.fromRGBO(253,116,99,  .9),
 900:Color.fromRGBO(253,116,99,  1),
};

MaterialColor colorPrimaryMaterial = MaterialColor(0XFFfd7463, colorCombination);