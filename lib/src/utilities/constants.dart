
const String appName = "FoodLab";

const String apiUrl = "https://cookbook-dev-backend.leam.ae";

const String LOGIN_SUCCESS = "loginSuccess";

const int SUCCESS_STATUS_CODE = 201;
const int API_SUCCESS_STATUS_CODE = 200;
const int FAILURE_CODE = 417;
